package cz.ti.elevator.gui;

import cz.ti.elevator.automaton.*;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

/**
 * Instances of the class {@code GUI} create a graphical user interface.
 * <p>
 * Created by Ladislav Procházka on 18.12.2018.
 * ladislav-prochazka@seznam.cz
 *
 * @author Ladislav Procházka
 */
public class GUI extends Application {
    /**
     * Elevator isnt on the floor
     */
    private static final Color STATE1 = Color.LIGHTGRAY;
    /**
     * Elevator is on the floor with close door
     */
    private static final Color STATE2 = Color.BLACK;
    /**
     * Elevator is on the floor and is opening the door
     */
    private static final Color STATE3 = Color.ORANGE;
    /**
     * Elevator is on the floor and have open the door
     */
    private static final Color STATE4 = Color.YELLOW;
    /**
     * Elevator is on the floor and is closing the door
     */
    private static final Color STATE5 = Color.RED;
    /**
     * Elevator passed through the floor
     */
    private static final Color STATE6 = Color.DARKGRAY;

    /**
     * Current instance of class {@code GUI}
     */
    private static GUI instance;

    /**
     * Elevator
     */
    private ObservableFSM elevator;

    /**
     * Display
     */
    private final TextField display = new TextField();

    /**
     * 1. floor
     */
    private final Floor floor1 = new Floor("1. floor");
    /**
     * 2. floor
     */
    private final Floor floor2 = new Floor("2. floor");
    /**
     * 3. floor
     */
    private final Floor floor3 = new Floor("3. floor");
    /**
     * 4. floor
     */
    private final Floor floor4 = new Floor("4. floor");

    /**
     * State door
     */
    private boolean openDoor = false;

    /**
     * Sets elevator
     *
     * @param elevator elevator
     */
    public GUI(ObservableFSM elevator) {
        this.elevator = elevator;
    }

    /**
     * Constructor calls the method {@code setInstance(GUI gui)}
     */
    public GUI() {
        setInstance(this);
    }

    /**
     * The method sets instance value
     *
     * @param gui current instance of class {@code GUI}
     */
    private static void setInstance(GUI gui) {
        instance = gui;
    }

    /**
     * Sets and returns instance of class {@code GUI}
     * @param fsm elevator
     * @return Current instance of class {@code GUI}
     */

    public synchronized static GUI getInstance(ObservableFSM fsm) {
        if (instance == null) {
            new Thread(() -> GUI.launch(GUI.class)).start();
            while (instance == null) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        instance.elevator = fsm;
        instance.setValue(instance.elevator.getStateOutput());
        instance.setEvents();

        // simulator.getSimulatiable().setCallback(instance);
        // instance.setSimulator(simulator);
        return instance;
    }

    /**
     * The method sets primary stage
     *
     * @param primaryStage primary stage
     */
    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Elevator");
        primaryStage.setMinHeight(360);
        primaryStage.setMinWidth(640);
        primaryStage.setScene(getScene());
        primaryStage.setOnCloseRequest(event -> System.exit(0));
        primaryStage.show();
    }

    /**
     * The method returns the scene of the stage
     *
     * @return instance of class {@code Scene}
     */
    public Scene getScene() {
        Scene scene = new Scene(getRoot(), 1280, 720);
        return scene;
    }

    /**
     * The method returns the root panel of the scene
     *
     * @return rootPane
     */
    private Parent getRoot() {
        BorderPane rootPane = new BorderPane();
        rootPane.setTop(getTopPane());
        rootPane.setCenter(getCenterPane());

//        setValue(elevator.getStateOutput());
//        setEvents();

        return rootPane;
    }

    /**
     * Returns top panel.
     *
     * @return instance of class {@code Node}
     */
    private Node getTopPane() {
        BorderPane topPane = new BorderPane();

        Button bReset = new Button("Reset");
        bReset.setOnAction(event -> {
            openDoor = false;
            elevator.reset();
        });
        bReset.setPrefHeight(50);
        bReset.setPrefWidth(100);

        display.setPrefHeight(20);
        display.setPrefWidth(200);
        display.setMaxWidth(200);
        display.setEditable(false);
        display.setAlignment(Pos.CENTER);
        display.setStyle("-fx-font-size: 50px;");

        topPane.setLeft(bReset);
        topPane.setCenter(display);
        topPane.setPadding(new Insets(10, 110, 10, 10));
        return topPane;
    }

    /**
     * Returns center panel.
     *
     * @return instance of class {@code Node}
     */
    private Node getCenterPane() {
        HBox centerPane = new HBox();

        centerPane.getChildren().addAll(getInnerPane(), getOutdoorPane());
        centerPane.setAlignment(Pos.TOP_CENTER);
        centerPane.setPadding(new Insets(0, 0, 20, 0));
        centerPane.setSpacing(20);
        centerPane.setStyle("-fx-font-size: 25");
        return centerPane;
    }

    /**
     * Returns internal elevator panel
     *
     * @return instance of class {@code Node}
     */
    private Node getInnerPane() {
        VBox packingElPane = new VBox();
        HBox elevatorPane = new HBox();
        VBox floorSelection = new VBox();

        Label titleElevatorPane = new Label("Inner panel");
        titleElevatorPane.setStyle("-fx-font-size: 35px;");

        floorSelection.getChildren().addAll(getButton("4", ElevatorInputAlphabet.G_4),
                getButton("3", ElevatorInputAlphabet.G_3), getButton("2", ElevatorInputAlphabet.G_2),
                getButton("1", ElevatorInputAlphabet.G_1));
        floorSelection.setSpacing(20);
        floorSelection.setStyle("-fx-font-size: 50px;");

        Button door = new Button("Door");
        door.setPrefHeight(100);
        door.setPrefWidth(150);
        door.setStyle("-fx-background-radius: 10%;");
        door.setOnAction(event -> elevator.push(ElevatorInputAlphabet.OPEN.name()));

        elevatorPane.getChildren().addAll(floorSelection, door);
        elevatorPane.setSpacing(20);

        packingElPane.getChildren().addAll(titleElevatorPane, elevatorPane);
        packingElPane.setAlignment(Pos.TOP_CENTER);
        packingElPane.setStyle("-fx-border-color: black");
        packingElPane.setPadding(new Insets(20));
        packingElPane.setSpacing(10);
        packingElPane.setPrefHeight(100);
        return packingElPane;
    }

    /**
     * Creates internal elevator button for control elevator
     *
     * @param title                 title button
     * @param elevatorInputAlphabet input function
     * @return instance of class {@code Node}
     */
    private Node getButton(String title, ElevatorInputAlphabet elevatorInputAlphabet) {
        Button button = new Button(title);
        button.setPrefHeight(100);
        button.setPrefWidth(100);
        button.setStyle("-fx-background-radius: 100%");
        button.setOnAction(event -> elevator.push(elevatorInputAlphabet.name()));
        return button;
    }

    /**
     * Returns outdoor elevator panel
     *
     * @return instance of class {@code Node}
     */
    private Node getOutdoorPane() {
        VBox outdoorPane = new VBox();

        Label titleOutdoorPane = new Label("Outdoor panel");
        titleOutdoorPane.setStyle("-fx-font-size: 35px;");

        outdoorPane.getChildren().addAll(titleOutdoorPane, floor4.getOutput(), floor3.getOutput(), floor2.getOutput(),
                floor1.getOutput());
        outdoorPane.setAlignment(Pos.TOP_CENTER);
        outdoorPane.setPadding(new Insets(20));
        outdoorPane.setStyle("-fx-border-color: black;");
        outdoorPane.setSpacing(15);
        return outdoorPane;
    }

    /**
     * Sets control buttons actions
     */
    private void setEvents() {
        floor1.getCall().setOnAction(event -> elevator.push(ElevatorInputAlphabet.C_1.name()));
        floor2.getCall().setOnAction(event -> elevator.push(ElevatorInputAlphabet.C_2.name()));
        floor3.getCall().setOnAction(event -> elevator.push(ElevatorInputAlphabet.C_3.name()));
        floor4.getCall().setOnAction(event -> elevator.push(ElevatorInputAlphabet.C_4.name()));

        floor1.getSensor().setOnAction(event -> elevator.push(ElevatorInputAlphabet.DI_1.name()));
        floor2.getSensor().setOnAction(event -> elevator.push(ElevatorInputAlphabet.DI_2.name()));
        floor3.getSensor().setOnAction(event -> elevator.push(ElevatorInputAlphabet.DI_3.name()));
        floor4.getSensor().setOnAction(event -> elevator.push(ElevatorInputAlphabet.DI_4.name()));

        elevator.setOnStateChangedListener(this::setValue);
    }

    /**
     * Sets the current values
     *
     * @param output output states
     */
    private void setValue(Object output) {
        System.out.println(output);

        Rectangle floor1S = floor1.getState();
        Rectangle floor2S = floor2.getState();
        Rectangle floor3S = floor3.getState();
        Rectangle floor4S = floor4.getState();

        if (output instanceof Output) {
            Output inputToGUI = (Output) output;
            if (inputToGUI.FLOOR_1) {
                display.setText("1");
                if (inputToGUI.MOTOR_UP || inputToGUI.MOTOR_DOWN) {
                    floor1S.setFill(STATE6);
                } else if (inputToGUI.DOOR_1_OPEN) {
                    floor1S.setFill(STATE3);
                    openDoor = true;
                } else if (inputToGUI.DOOR_1_CLOSE) {
                    floor1S.setFill(STATE5);
                    openDoor = false;
                } else if (openDoor) {
                    floor1S.setFill(STATE4);
                } else {
                    floor1S.setFill(STATE2);
                }
                Rectangle[] floorsState = {floor2S, floor3S, floor4S};
                setState(floorsState, STATE1);
            } else if (inputToGUI.FLOOR_2) {
                display.setText("2");
                if (inputToGUI.MOTOR_UP || inputToGUI.MOTOR_DOWN) {
                    floor2S.setFill(STATE6);
                } else if (inputToGUI.DOOR_2_OPEN) {
                    floor2S.setFill(STATE3);
                    openDoor = true;
                } else if (inputToGUI.DOOR_2_CLOSE) {
                    floor2S.setFill(STATE5);
                    openDoor = false;
                } else if (openDoor) {
                    floor2S.setFill(STATE4);
                } else {
                    floor2S.setFill(STATE2);
                }
                Rectangle[] floorsState = {floor1S, floor3S, floor4S};
                setState(floorsState, STATE1);
            } else if (inputToGUI.FLOOR_3) {
                display.setText("3");
                if (inputToGUI.MOTOR_UP || inputToGUI.MOTOR_DOWN) {
                    floor3S.setFill(STATE6);
                } else if (inputToGUI.DOOR_3_OPEN) {
                    floor3S.setFill(STATE3);
                    openDoor = true;
                } else if (inputToGUI.DOOR_3_CLOSE) {
                    floor3S.setFill(STATE5);
                    openDoor = false;
                } else if (openDoor) {
                    floor3S.setFill(STATE4);
                } else {
                    floor3S.setFill(STATE2);
                }
                Rectangle[] floorsState = {floor1S, floor2S, floor4S};
                setState(floorsState, STATE1);
            } else if (inputToGUI.FLOOR_4) {
                display.setText("4");
                if (inputToGUI.MOTOR_UP || inputToGUI.MOTOR_DOWN) {
                    floor4S.setFill(STATE6);
                } else if (inputToGUI.DOOR_4_OPEN) {
                    floor4S.setFill(STATE3);
                    openDoor = true;
                } else if (inputToGUI.DOOR_4_CLOSE) {
                    floor4S.setFill(STATE5);
                    openDoor = false;
                } else if (openDoor) {
                    floor4S.setFill(STATE4);
                } else {
                    floor4S.setFill(STATE2);
                }
                Rectangle[] floorsState = {floor1S, floor2S, floor3S};
                setState(floorsState, STATE1);
            }
        }
    }

    /**
     * Sets input rectangles state
     *
     * @param rectangles rectangles
     * @param state      color state
     */
    private void setState(Rectangle[] rectangles, Color state) {
        for (Rectangle rectangle : rectangles) {
            rectangle.setFill(state);
        }
    }
}
