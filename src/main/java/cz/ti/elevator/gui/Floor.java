package cz.ti.elevator.gui;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.shape.Rectangle;

/**
 * Instances of the class {@code GUI} represent the floor.
 * <p>
 * Created by Ladislav Procházka on 18.12.2018.
 * ladislav-prochazka@seznam.cz
 *
 * @author Ladislav Procházka
 */
public class Floor {
    /**
     * Title of floor
     */
    private final Label title = new Label();
    /**
     * State of floor
     */
    private final Rectangle state = new Rectangle(100, 100);
    /**
     * Button calling elevator
     */
    private final Button call = new Button("Call");
    /**
     * Sensor on the door
     */
    private final Button sensor = new Button("Sensor");

    /**
     * Sets all constants
     *
     * @param title
     */
    public Floor(String title) {
        this.title.setText(title);

        call.setPrefHeight(100);
        call.setPrefWidth(150);
        call.setStyle("-fx-background-radius: 10%;");
        ;
        sensor.setPrefHeight(100);
        sensor.setPrefWidth(150);
        sensor.setStyle("-fx-background-radius: 10%;");
    }

    /**
     * Title of floor
     *
     * @return instance of class {@code Label}
     */
    public Label getTitle() {
        return title;
    }

    /**
     * State of floor
     *
     * @return instance of class {@code Rectangle}
     */
    public Rectangle getState() {
        return state;
    }

    /**
     * Button calling elevator
     *
     * @return instance of class {@code Button}
     */
    public Button getCall() {
        return call;
    }

    /**
     * Sensor on the door
     *
     * @return instance of class {@code Button}
     */
    public Button getSensor() {
        return sensor;
    }

    /**
     * Returns the floor representation
     *
     * @return instance of class {@code Node}
     */
    public Node getOutput() {
        HBox floor = new HBox();
        floor.getChildren().addAll(title, state, call, sensor);
        floor.setSpacing(20);
        floor.setAlignment(Pos.CENTER);
        return floor;
    }
}

