package cz.ti.elevator.simulation;

import cz.ti.elevator.automaton.*;

/**
 * Simulation for auto generating some inputs like sensors
 * <p>
 * Created by Martin Forejt on 18.12.2018.
 * me@martinforejt.cz
 *
 * @author Martin Forejt
 */
public class Simulation implements ObservableFSM {

    /**
     * Delay how long elevator goes from one floor to another
     */
    private static final int ONE_FLOOR_DELAY = 2000;
    /**
     * Dalay how long it takes to open/close doors
     */
    private static final int DOOR_OPEN_CLOSE = 2000;

    /**
     * FSM
     */
    private final FSM fsm;
    /**
     * On state change listener
     */
    private OnStateChangedListener onStateChangedListener;

    /**
     * Constructs new {@link Simulation}
     * and sets {@link ObservableFSM#setOnStateChangedListener(OnStateChangedListener)}
     *
     * @param fsm fsm
     */
    public Simulation(ObservableFSM fsm) {
        this.fsm = fsm;

        fsm.setOnStateChangedListener((output) -> {
            if (output instanceof Output) {
                checkStateOutput((Output) output);
            }
            if (onStateChangedListener != null) {
                onStateChangedListener.onStateChanged(output);
            }
        });
    }

    /**
     * Push input to {@link #fsm}
     *
     * @param input input
     * @return Mealy output
     */
    @Override
    public int push(String input) {
        return fsm.push(input);
    }

    /**
     * Check state output and may schedule some "push tasks"
     *
     * @param o Moore output
     */
    private void checkStateOutput(Output o) {
        if (o.MOTOR_DOWN) {
            if (o.FLOOR_2) pushAfter(ElevatorInputAlphabet.IN_1.name(), ONE_FLOOR_DELAY);
            else if (o.FLOOR_3) pushAfter(ElevatorInputAlphabet.IN_2.name(), ONE_FLOOR_DELAY);
            else if (o.FLOOR_4) pushAfter(ElevatorInputAlphabet.IN_3.name(), ONE_FLOOR_DELAY);
        }

        if (o.MOTOR_UP) {
            if (o.FLOOR_1) pushAfter(ElevatorInputAlphabet.IN_2.name(), ONE_FLOOR_DELAY);
            else if (o.FLOOR_2) pushAfter(ElevatorInputAlphabet.IN_3.name(), ONE_FLOOR_DELAY);
            else if (o.FLOOR_3) pushAfter(ElevatorInputAlphabet.IN_4.name(), ONE_FLOOR_DELAY);
        }

        if (o.DOOR_1_OPEN) {
            pushAfter(ElevatorInputAlphabet.DO_1.name(), DOOR_OPEN_CLOSE);
        } else if (o.DOOR_1_CLOSE) {
            pushAfter(ElevatorInputAlphabet.DC_1.name(), DOOR_OPEN_CLOSE);
        }

        if (o.DOOR_2_OPEN) {
            pushAfter(ElevatorInputAlphabet.DO_2.name(), DOOR_OPEN_CLOSE);
        } else if (o.DOOR_2_CLOSE) {
            pushAfter(ElevatorInputAlphabet.DC_2.name(), DOOR_OPEN_CLOSE);
        }

        if (o.DOOR_3_OPEN) {
            pushAfter(ElevatorInputAlphabet.DO_3.name(), DOOR_OPEN_CLOSE);
        } else if (o.DOOR_3_CLOSE) {
            pushAfter(ElevatorInputAlphabet.DC_3.name(), DOOR_OPEN_CLOSE);
        }

        if (o.DOOR_4_OPEN) {
            pushAfter(ElevatorInputAlphabet.DO_4.name(), DOOR_OPEN_CLOSE);
        } else if (o.DOOR_4_CLOSE) {
            pushAfter(ElevatorInputAlphabet.DC_4.name(), DOOR_OPEN_CLOSE);
        }
    }

    @Override
    public void setOnStateChangedListener(OnStateChangedListener onStateChangedListener) {
        this.onStateChangedListener = onStateChangedListener;
    }

    /**
     * Return Moore output
     *
     * @return Moore output
     */
    @Override
    public Object getStateOutput() {
        return fsm.getStateOutput();
    }

    /**
     * Resets {@link #fsm}
     */
    @Override
    public void reset() {
        fsm.reset();
        if (getStateOutput() instanceof Output) {
            checkStateOutput((Output) getStateOutput());
        }
        if (onStateChangedListener != null) {
            onStateChangedListener.onStateChanged(getStateOutput());
        }
    }

    /**
     * Push {@code input} after {@code delay} ms
     *
     * @param input input
     * @param delay delay in ms
     */
    private void pushAfter(String input, int delay) {
        new java.util.Timer().schedule(
                new java.util.TimerTask() {
                    @Override
                    public void run() {
                        push(input);
                    }
                },
                delay
        );
    }
}
