package cz.ti.elevator.automaton;

import org.apache.commons.lang3.tuple.Pair;

import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Represents state in {@link ElevatorFSM}
 * <p>
 * Created by Martin Forejt on 16.12.2018.
 * me@martinforejt.cz
 *
 * @author Martin Forejt
 */
public abstract class State {

    /**
     * Builds new state
     *
     * @param name                 state name
     * @param pairAlphabetFunction transition function
     * @param outputSupplier       Moore output function
     * @return state
     */
    static State build(String name, Function<ElevatorInputAlphabet, Pair<State, Integer>> pairAlphabetFunction, Supplier<Output> outputSupplier) {
        return new State() {
            @Override
            public Pair<State, Integer> transition(ElevatorInputAlphabet input) {
                return pairAlphabetFunction.apply(input);
            }

            @Override
            public Output getOutput() {
                Output o = outputSupplier.get();
                o.STATE = name;
                return o;
            }

            @Override
            public String getName() {
                return name;
            }
        };
    }

    /**
     * Transition function
     *
     * @param input input from {@link ElevatorInputAlphabet} alphabet
     * @return Pair represents new state and Mealy output
     */
    public abstract Pair<State, Integer> transition(ElevatorInputAlphabet input);

    /**
     * Returns Moore output
     *
     * @return Moore output
     */
    public abstract Output getOutput();

    /**
     * Returns state name
     * The name is also available in Moore output {@link #getOutput()} - {@link Output#STATE}
     *
     * @return name
     */
    public abstract String getName();
}
