package cz.ti.elevator.automaton;

/**
 * Input alphabet for {@link ElevatorFSM}
 * <p>
 * Created by Martin Forejt on 16.12.2018.
 * me@martinforejt.cz
 *
 * @author Martin Forejt
 */
public enum ElevatorInputAlphabet {

    /** Go to floor 1 button in elevator */
    G_1,
    /** Go to floor 2 button in elevator */
    G_2,
    /** Go to floor 3 button in elevator */
    G_3,
    /** Go to floor 4 button in elevator */
    G_4,

    /** Call elevator button in floor 1 */
    C_1,
    /** Call elevator button in floor 2 */
    C_2,
    /** Call elevator button in floor 3 */
    C_3,
    /** Call elevator button in floor 4 */
    C_4,

    /** Floor sensor in floor 1 */
    IN_1,
    /** Floor sensor in floor 2 */
    IN_2,
    /** Floor sensor in floor 3 */
    IN_3,
    /** Floor sensor in floor 4 */
    IN_4,

    /** Door opened sensor in floor 1 */
    DO_1,
    /** Door opened sensor in floor 2 */
    DO_2,
    /** Door opened sensor in floor 3 */
    DO_3,
    /** Door opened sensor in floor 4 */
    DO_4,

    /** Door closed sensor in floor 1 */
    DC_1,
    /** Door closed sensor in floor 2 */
    DC_2,
    /** Door closed sensor in floor 3 */
    DC_3,
    /** Door closed sensor in floor 4 */
    DC_4,

    /** Door interrupted sensor in floor 1 */
    DI_1,
    /** Door interrupted sensor in floor 2 */
    DI_2,
    /** Door interrupted sensor in floor 3 */
    DI_3,
    /** Door interrupted sensor in floor 4 */
    DI_4,

    /** Open button in elevator */
    OPEN,

    /** Time elapsed */
    T,
}
