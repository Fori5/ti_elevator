package cz.ti.elevator.automaton;

/**
 * Represents Moore state output in {@link ElevatorFSM}
 * <p>
 * Created by Martin Forejt on 17.12.2018.
 * me@martinforejt.cz
 *
 * @author Martin Forejt
 */
public class Output {

    /** Holds current state name */
    public String STATE = "";

    /** Motor UP */
    public boolean MOTOR_UP = false;
    /** Motor DOWN */
    public boolean MOTOR_DOWN = false;

    /** Door 1 motor OPEN */
    public boolean DOOR_1_OPEN = false;
    /** Door 1 motor CLOSE */
    public boolean DOOR_1_CLOSE = false;
    /** Door 2 motor OPEN */
    public boolean DOOR_2_OPEN = false;
    /** Door 2 motor CLOSE */
    public boolean DOOR_2_CLOSE = false;
    /** Door 3 motor OPEN */
    public boolean DOOR_3_OPEN = false;
    /** Door 3 motor CLOSE */
    public boolean DOOR_3_CLOSE = false;
    /** Door 4 motor OPEN */
    public boolean DOOR_4_OPEN = false;
    /** Door 4 motor CLOSE */
    public boolean DOOR_4_CLOSE = false;

    /** Is light in elevator on */
    public boolean LIGHT = false;

    /** Is elevator "near" the floor 1 */
    public boolean FLOOR_1 = false;
    /** Is elevator "near" the floor 2 */
    public boolean FLOOR_2 = false;
    /** Is elevator "near" the floor 3 */
    public boolean FLOOR_3 = false;
    /** Is elevator "near" the floor 4 */
    public boolean FLOOR_4 = false;

    @Override
    public String toString() {
        return "Output{" +
                "STATE='" + STATE + '\'' +
                ", MOTOR_UP=" + MOTOR_UP +
                ", MOTOR_DOWN=" + MOTOR_DOWN +
                ", DOOR_1_OPEN=" + DOOR_1_OPEN +
                ", DOOR_1_CLOSE=" + DOOR_1_CLOSE +
                ", DOOR_2_OPEN=" + DOOR_2_OPEN +
                ", DOOR_2_CLOSE=" + DOOR_2_CLOSE +
                ", DOOR_3_OPEN=" + DOOR_3_OPEN +
                ", DOOR_3_CLOSE=" + DOOR_3_CLOSE +
                ", DOOR_4_OPEN=" + DOOR_4_OPEN +
                ", DOOR_4_CLOSE=" + DOOR_4_CLOSE +
                ", LIGHT=" + LIGHT +
                ", FLOOR_1=" + FLOOR_1 +
                ", FLOOR_2=" + FLOOR_2 +
                ", FLOOR_3=" + FLOOR_3 +
                ", FLOOR_4=" + FLOOR_4 +
                '}';
    }
}
