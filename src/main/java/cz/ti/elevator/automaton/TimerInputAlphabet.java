package cz.ti.elevator.automaton;

/**
 * Input alphabet for {@link TimerFSM}
 * <p>
 * Created by Martin Forejt on 18.12.2018.
 * me@martinforejt.cz
 *
 * @author Martin Forejt
 */
public enum TimerInputAlphabet {

    /**
     * CLK pulse
     */
    CLK,
    /**
     * 0 seconds = do not set timer
     */
    T_0,
    /**
     * 5 steps timer
     */
    T_5,
    /**
     * 15 steps timer
     */
    T_15;

    /**
     * Returns alphabet "char" by int
     *
     * @param steps steps to wait
     * @return alphabet "char"
     */
    public static TimerInputAlphabet byTime(int steps) {
        switch (steps) {
            case 5:
                return T_5;
            case 15:
                return T_15;
            default:
                return T_0;
        }
    }
}
