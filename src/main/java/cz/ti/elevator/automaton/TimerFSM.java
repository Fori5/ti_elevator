package cz.ti.elevator.automaton;

/**
 * Represents FSM which generates Mealy output (when transition from state 1 to 0)
 * after x seconds depending on input
 * <p>
 * Created by Martin Forejt on 18.12.2018.
 * me@martinforejt.cz
 *
 * @author Martin Forejt
 */
public class TimerFSM implements FSM {

    /**
     * Current state
     */
    private int state = 0;

    /**
     * Constructs new {@link TimerFSM}
     * and reset it
     */
    TimerFSM() {
        reset();
    }

    /**
     * Push input from {@link TimerInputAlphabet}
     *
     * @param input input
     * @return Mealy output 0: time not out, 1: time out
     */
    @Override
    public int push(String input) {
        TimerInputAlphabet alphabetItem;
        try {
            alphabetItem = TimerInputAlphabet.valueOf(input);
        } catch (IllegalArgumentException e) {
            return BAD_INPUT;
        }

        switch (alphabetItem) {
            case CLK:
                state--;
                if (state == 0) {
                    return 1;
                }
                break;
            case T_5:
                state = 5;
                break;
            case T_15:
                state = 15;
                break;
        }

        return 0;
    }

    /**
     * No Moore output defined
     *
     * @return null
     */
    @Override
    public Object getStateOutput() {
        return null;
    }

    /**
     * Sets state to 0
     */
    @Override
    public void reset() {
        state = 0;
    }
}
