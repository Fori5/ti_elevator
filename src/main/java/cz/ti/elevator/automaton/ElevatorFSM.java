package cz.ti.elevator.automaton;

import org.apache.commons.lang3.tuple.Pair;

/**
 * Elevator FSM
 * <p>
 * Created by Martin Forejt on 16.12.2018.
 * me@martinforejt.cz
 *
 * @author Martin Forejt
 */
@SuppressWarnings("Duplicates")
public class ElevatorFSM implements FSM {

    /**
     * Represents state when elevator is in floor 1-4,
     * doors are closed, all motors are off, light is off
     */
    private static State IDLE_1, IDLE_2, IDLE_3, IDLE_4;
    /**
     * Represents state like {@link #IDLE_1}-{@link #IDLE_4},
     * but keep light on
     */
    private static State READY_1, READY_2, READY_3, READY_4;
    /**
     * Represents state when elevator is moving from floor 1 to another
     * doors are close, motor up/down is on, light is on
     */
    private static State THROW_1_TO_2, THROW_1_TO_3, THROW_1_TO_4;
    /**
     * Represents state when elevator is moving from floor 2 to another
     * doors are close, motor up/down is on, light is on
     */
    private static State THROW_2_TO_1, THROW_2_TO_3, THROW_2_TO_4;
    /**
     * Represents state when elevator is moving from floor 3 to another
     * doors are close, motor up/down is on, light is on
     */
    private static State THROW_3_TO_1, THROW_3_TO_2, THROW_3_TO_4;
    /**
     * Represents state when elevator is moving from floor 4 to another
     * doors are close, motor up/down is on, light is on
     */
    private static State THROW_4_TO_1, THROW_4_TO_2, THROW_4_TO_3;
    /**
     * Represents state when elevator reached required floor
     * motor stops, doors are opening, light is on
     */
    private static State FINISH_1, FINISH_2, FINISH_3, FINISH_4;
    /**
     * Represents state when loading/unloading is performed
     * motors are off, doors are opened, light is on
     * When transition to this state, Mealy output may be generated (defined as seconds to wait)
     */
    private static State WAIT_1, WAIT_2, WAIT_3, WAIT_4;
    /**
     * Represents state when loading/unloading is finished (wait time is off)
     * motors are off, doors are closing, light is on
     */
    private static State CLOSE_1, CLOSE_2, CLOSE_3, CLOSE_4;

    /**
     * Current Moore state
     */
    private State currentState;

    /**
     * Constructs new {@link ElevatorFSM} and resets it
     */
    ElevatorFSM() {
        reset();
    }

    /**
     * Push input to elevator FSM
     * Check if input is in elevator's alphabet ({@link ElevatorInputAlphabet})
     * Using current's state transition function moves to another state and generate Mealy output
     *
     * @param input input
     * @return Mealy output
     */
    @Override
    public int push(String input) {
        ElevatorInputAlphabet alphabetItem;
        try {
            alphabetItem = ElevatorInputAlphabet.valueOf(input);
        } catch (IllegalArgumentException e) {
            return BAD_INPUT;
        }

        Pair<State, Integer> res = currentState.transition(alphabetItem);
        currentState = res.getKey();
        return res.getValue() != null ? res.getValue() : 0;
    }

    /**
     * Returns current state Moore output
     *
     * @return Moore output
     */
    @Override
    public Output getStateOutput() {
        return currentState.getOutput();
    }

    /**
     * Set state to {@link #IDLE_1}
     */
    @Override
    public void reset() {
        currentState = IDLE_1;
    }

    // Init all available states here:
    static {

        /* ---------------- IDLE STATES ----------------------- */

        IDLE_1 = State.build("IDLE_1", alphabet -> {
                    switch (alphabet) {
                        case G_1:
                        case C_1:
                        case OPEN:
                            return Pair.of(FINISH_1, null);
                        case G_2:
                        case C_2:
                            return Pair.of(THROW_1_TO_2, null);
                        case G_3:
                        case C_3:
                            return Pair.of(THROW_1_TO_3, null);
                        case G_4:
                        case C_4:
                            return Pair.of(THROW_1_TO_4, null);
                        default:
                            return Pair.of(IDLE_1, null);
                    }
                },
                () -> {
                    Output o = new Output();
                    o.FLOOR_1 = true;
                    return o;
                }
        );

        IDLE_2 = State.build("IDLE_2", alphabet -> {
                    switch (alphabet) {
                        case G_1:
                        case C_1:
                            return Pair.of(THROW_2_TO_1, null);
                        case G_2:
                        case C_2:
                        case OPEN:
                            return Pair.of(FINISH_2, null);
                        case G_3:
                        case C_3:
                            return Pair.of(THROW_2_TO_3, null);
                        case G_4:
                        case C_4:
                            return Pair.of(THROW_2_TO_4, null);
                        default:
                            return Pair.of(IDLE_2, null);
                    }
                },
                () -> {
                    Output o = new Output();
                    o.FLOOR_2 = true;
                    return o;
                }
        );

        IDLE_3 = State.build("IDLE_3", alphabet -> {
                    switch (alphabet) {
                        case G_1:
                        case C_1:
                            return Pair.of(THROW_3_TO_1, null);
                        case G_2:
                        case C_2:
                            return Pair.of(THROW_3_TO_2, null);
                        case G_3:
                        case C_3:
                        case OPEN:
                            return Pair.of(FINISH_3, null);
                        case G_4:
                        case C_4:
                            return Pair.of(THROW_3_TO_4, null);
                        default:
                            return Pair.of(IDLE_3, null);
                    }
                },
                () -> {
                    Output o = new Output();
                    o.FLOOR_3 = true;
                    return o;
                }
        );

        IDLE_4 = State.build("IDLE_4", alphabet -> {
                    switch (alphabet) {
                        case G_1:
                        case C_1:
                            return Pair.of(THROW_4_TO_1, null);
                        case G_2:
                        case C_2:
                            return Pair.of(THROW_4_TO_2, null);
                        case G_3:
                        case C_3:
                            return Pair.of(THROW_4_TO_3, null);
                        case G_4:
                        case C_4:
                        case OPEN:
                            return Pair.of(FINISH_4, null);
                        default:
                            return Pair.of(IDLE_4, null);
                    }
                },
                () -> {
                    Output o = new Output();
                    o.FLOOR_4 = true;
                    return o;
                }
        );

        /* ---------------- READY STATES ----------------------- */

        READY_1 = State.build("READY_1", alphabet -> {
                    switch (alphabet) {
                        case G_1:
                        case C_1:
                        case OPEN:
                            return Pair.of(FINISH_1, null);
                        case G_2:
                        case C_2:
                            return Pair.of(THROW_1_TO_2, null);
                        case G_3:
                        case C_3:
                            return Pair.of(THROW_1_TO_3, null);
                        case G_4:
                        case C_4:
                            return Pair.of(THROW_1_TO_4, null);
                        case T:
                            return Pair.of(IDLE_1, null);
                        default:
                            return Pair.of(READY_1, null);
                    }
                },
                () -> {
                    Output o = new Output();
                    o.FLOOR_1 = true;
                    o.LIGHT = true;
                    return o;
                }
        );

        READY_2 = State.build("READY_2", alphabet -> {
                    switch (alphabet) {
                        case G_1:
                        case C_1:
                            return Pair.of(THROW_2_TO_1, null);
                        case G_2:
                        case C_2:
                        case OPEN:
                            return Pair.of(FINISH_2, null);
                        case G_3:
                        case C_3:
                            return Pair.of(THROW_2_TO_3, null);
                        case G_4:
                        case C_4:
                            return Pair.of(THROW_2_TO_4, null);
                        case T:
                            return Pair.of(IDLE_2, null);
                        default:
                            return Pair.of(READY_2, null);
                    }
                },
                () -> {
                    Output o = new Output();
                    o.FLOOR_2 = true;
                    o.LIGHT = true;
                    return o;
                }
        );

        READY_3 = State.build("READY_3", alphabet -> {
                    switch (alphabet) {
                        case G_1:
                        case C_1:
                            return Pair.of(THROW_3_TO_1, null);
                        case G_2:
                        case C_2:
                            return Pair.of(THROW_3_TO_2, null);
                        case G_3:
                        case C_3:
                        case OPEN:
                            return Pair.of(FINISH_3, null);
                        case G_4:
                        case C_4:
                            return Pair.of(THROW_3_TO_4, null);
                        case T:
                            return Pair.of(IDLE_3, null);
                        default:
                            return Pair.of(READY_3, null);
                    }
                },
                () -> {
                    Output o = new Output();
                    o.FLOOR_3 = true;
                    o.LIGHT = true;
                    return o;
                }
        );

        READY_4 = State.build("READY_4", alphabet -> {
                    switch (alphabet) {
                        case G_1:
                        case C_1:
                            return Pair.of(THROW_4_TO_1, null);
                        case G_2:
                        case C_2:
                            return Pair.of(THROW_4_TO_2, null);
                        case G_3:
                        case C_3:
                            return Pair.of(THROW_4_TO_3, null);
                        case G_4:
                        case C_4:
                        case OPEN:
                            return Pair.of(FINISH_4, null);
                        case T:
                            return Pair.of(IDLE_4, null);
                        default:
                            return Pair.of(READY_4, null);
                    }
                },
                () -> {
                    Output o = new Output();
                    o.FLOOR_4 = true;
                    o.LIGHT = true;
                    return o;
                }
        );

        /* ---------------- GO THROW STATES ----------------------- */

        THROW_1_TO_2 = State.build("THROW_1_TO_2", alphabet -> {
                    switch (alphabet) {
                        case IN_2:
                            return Pair.of(FINISH_2, null);
                        default:
                            return Pair.of(THROW_1_TO_2, null);
                    }
                },
                () -> {
                    Output o = new Output();
                    o.FLOOR_1 = true;
                    o.LIGHT = true;
                    o.MOTOR_UP = true;
                    return o;
                }
        );
        THROW_1_TO_3 = State.build("THROW_1_TO_3", alphabet -> {
                    switch (alphabet) {
                        case IN_2:
                            return Pair.of(THROW_2_TO_3, null);
                        case IN_3:
                            return Pair.of(FINISH_3, null);
                        default:
                            return Pair.of(THROW_1_TO_3, null);
                    }
                },
                () -> {
                    Output o = new Output();
                    o.FLOOR_1 = true;
                    o.LIGHT = true;
                    o.MOTOR_UP = true;
                    return o;
                }
        );
        THROW_1_TO_4 = State.build("THROW_1_TO_4", alphabet -> {
                    switch (alphabet) {
                        case IN_2:
                            return Pair.of(THROW_2_TO_4, null);
                        case IN_3:
                            return Pair.of(THROW_3_TO_4, null);
                        case IN_4:
                            return Pair.of(FINISH_4, null);
                        default:
                            return Pair.of(THROW_1_TO_4, null);
                    }
                },
                () -> {
                    Output o = new Output();
                    o.FLOOR_1 = true;
                    o.LIGHT = true;
                    o.MOTOR_UP = true;
                    return o;
                }
        );

        THROW_2_TO_1 = State.build("THROW_2_TO_1", alphabet -> {
                    switch (alphabet) {
                        case IN_1:
                            return Pair.of(FINISH_1, null);
                        default:
                            return Pair.of(THROW_2_TO_1, null);
                    }
                },
                () -> {
                    Output o = new Output();
                    o.FLOOR_2 = true;
                    o.LIGHT = true;
                    o.MOTOR_DOWN = true;
                    return o;
                }
        );
        THROW_2_TO_3 = State.build("THROW_2_TO_3", alphabet -> {
                    switch (alphabet) {
                        case IN_3:
                            return Pair.of(FINISH_3, null);
                        default:
                            return Pair.of(THROW_2_TO_3, null);
                    }
                },
                () -> {
                    Output o = new Output();
                    o.FLOOR_2 = true;
                    o.LIGHT = true;
                    o.MOTOR_UP = true;
                    return o;
                }
        );
        THROW_2_TO_4 = State.build("THROW_2_TO_4", alphabet -> {
                    switch (alphabet) {
                        case IN_3:
                            return Pair.of(THROW_3_TO_4, null);
                        case IN_4:
                            return Pair.of(FINISH_4, null);
                        default:
                            return Pair.of(THROW_2_TO_4, null);
                    }
                },
                () -> {
                    Output o = new Output();
                    o.FLOOR_2 = true;
                    o.LIGHT = true;
                    o.MOTOR_UP = true;
                    return o;
                }
        );
        THROW_3_TO_1 = State.build("THROW_3_TO_1", alphabet -> {
                    switch (alphabet) {
                        case IN_2:
                            return Pair.of(THROW_2_TO_1, null);
                        case IN_1:
                            return Pair.of(FINISH_1, null);
                        default:
                            return Pair.of(THROW_3_TO_1, null);
                    }
                },
                () -> {
                    Output o = new Output();
                    o.FLOOR_3 = true;
                    o.LIGHT = true;
                    o.MOTOR_DOWN = true;
                    return o;
                }
        );
        THROW_3_TO_2 = State.build("THROW_3_TO_2", alphabet -> {
                    switch (alphabet) {
                        case IN_2:
                            return Pair.of(FINISH_2, null);
                        default:
                            return Pair.of(THROW_3_TO_2, null);
                    }
                },
                () -> {
                    Output o = new Output();
                    o.FLOOR_3 = true;
                    o.LIGHT = true;
                    o.MOTOR_DOWN = true;
                    return o;
                }
        );
        THROW_3_TO_4 = State.build("THROW_3_TO_4", alphabet -> {
                    switch (alphabet) {
                        case IN_4:
                            return Pair.of(FINISH_4, null);
                        default:
                            return Pair.of(THROW_3_TO_4, null);
                    }
                },
                () -> {
                    Output o = new Output();
                    o.FLOOR_3 = true;
                    o.LIGHT = true;
                    o.MOTOR_UP = true;
                    return o;
                }
        );

        THROW_4_TO_1 = State.build("THROW_4_TO_1", alphabet -> {
                    switch (alphabet) {
                        case IN_3:
                            return Pair.of(THROW_3_TO_1, null);
                        case IN_2:
                            return Pair.of(THROW_2_TO_1, null);
                        case IN_1:
                            return Pair.of(FINISH_1, null);
                        default:
                            return Pair.of(THROW_4_TO_1, null);
                    }
                },
                () -> {
                    Output o = new Output();
                    o.FLOOR_4 = true;
                    o.LIGHT = true;
                    o.MOTOR_DOWN = true;
                    return o;
                }
        );
        THROW_4_TO_2 = State.build("THROW_4_TO_2", alphabet -> {
                    switch (alphabet) {
                        case IN_3:
                            return Pair.of(THROW_3_TO_2, null);
                        case IN_2:
                            return Pair.of(FINISH_2, null);
                        default:
                            return Pair.of(THROW_4_TO_2, null);
                    }
                },
                () -> {
                    Output o = new Output();
                    o.FLOOR_4 = true;
                    o.LIGHT = true;
                    o.MOTOR_DOWN = true;
                    return o;
                }
        );
        THROW_4_TO_3 = State.build("THROW_4_TO_3", alphabet -> {
                    switch (alphabet) {
                        case IN_3:
                            return Pair.of(FINISH_3, null);
                        default:
                            return Pair.of(THROW_4_TO_3, null);
                    }
                },
                () -> {
                    Output o = new Output();
                    o.FLOOR_4 = true;
                    o.LIGHT = true;
                    o.MOTOR_DOWN = true;
                    return o;
                }
        );

        /* ---------------- FINISH STATES ----------------------- */

        FINISH_1 = State.build("FINISH_1", alphabet -> {
                    switch (alphabet) {
                        case DO_1:
                            return Pair.of(WAIT_1, 5);
                        default:
                            return Pair.of(FINISH_1, null);
                    }
                },
                () -> {
                    Output o = new Output();
                    o.FLOOR_1 = true;
                    o.LIGHT = true;
                    o.DOOR_1_OPEN = true;
                    return o;
                }
        );
        FINISH_2 = State.build("FINISH_2", alphabet -> {
                    switch (alphabet) {
                        case DO_2:
                            return Pair.of(WAIT_2, 5);
                        default:
                            return Pair.of(FINISH_2, null);
                    }
                },
                () -> {
                    Output o = new Output();
                    o.FLOOR_2 = true;
                    o.LIGHT = true;
                    o.DOOR_2_OPEN = true;
                    return o;
                }
        );
        FINISH_3 = State.build("FINISH_3", alphabet -> {
                    switch (alphabet) {
                        case DO_3:
                            return Pair.of(WAIT_3, 5);
                        default:
                            return Pair.of(FINISH_3, null);
                    }
                },
                () -> {
                    Output o = new Output();
                    o.FLOOR_3 = true;
                    o.LIGHT = true;
                    o.DOOR_3_OPEN = true;
                    return o;
                }
        );
        FINISH_4 = State.build("FINISH_4", alphabet -> {
                    switch (alphabet) {
                        case DO_4:
                            return Pair.of(WAIT_4, 5);
                        default:
                            return Pair.of(FINISH_4, null);
                    }
                },
                () -> {
                    Output o = new Output();
                    o.FLOOR_4 = true;
                    o.LIGHT = true;
                    o.DOOR_4_OPEN = true;
                    return o;
                }
        );

        /* ---------------- WAIT STATES ----------------------- */

        WAIT_1 = State.build("WAIT_1", alphabet -> {
                    switch (alphabet) {
                        case T:
                            return Pair.of(CLOSE_1, null);
                        default:
                            return Pair.of(WAIT_1, null);
                    }
                },
                () -> {
                    Output o = new Output();
                    o.FLOOR_1 = true;
                    o.LIGHT = true;
                    return o;
                }
        );
        WAIT_2 = State.build("WAIT_2", alphabet -> {
                    switch (alphabet) {
                        case T:
                            return Pair.of(CLOSE_2, null);
                        default:
                            return Pair.of(WAIT_2, null);
                    }
                },
                () -> {
                    Output o = new Output();
                    o.FLOOR_2 = true;
                    o.LIGHT = true;
                    return o;
                }
        );
        WAIT_3 = State.build("WAIT_3", alphabet -> {
                    switch (alphabet) {
                        case T:
                            return Pair.of(CLOSE_3, null);
                        default:
                            return Pair.of(WAIT_3, null);
                    }
                },
                () -> {
                    Output o = new Output();
                    o.FLOOR_3 = true;
                    o.LIGHT = true;
                    return o;
                }
        );
        WAIT_4 = State.build("WAIT_4", alphabet -> {
                    switch (alphabet) {
                        case T:
                            return Pair.of(CLOSE_4, null);
                        default:
                            return Pair.of(WAIT_4, null);
                    }
                },
                () -> {
                    Output o = new Output();
                    o.FLOOR_4 = true;
                    o.LIGHT = true;
                    return o;
                }
        );

        /* ---------------- CLOSE STATES ----------------------- */

        CLOSE_1 = State.build("CLOSE_1", alphabet -> {
                    switch (alphabet) {
                        case DC_1:
                            return Pair.of(READY_1, 15);
                        case DI_1:
                        case OPEN:
                        case C_1:
                            return Pair.of(FINISH_1, null);
                        default:
                            return Pair.of(CLOSE_1, null);
                    }
                },
                () -> {
                    Output o = new Output();
                    o.FLOOR_1 = true;
                    o.LIGHT = true;
                    o.DOOR_1_CLOSE = true;
                    return o;
                }
        );
        CLOSE_2 = State.build("CLOSE_2", alphabet -> {
                    switch (alphabet) {
                        case DC_2:
                            return Pair.of(READY_2, 15);
                        case DI_2:
                        case OPEN:
                        case C_2:
                            return Pair.of(FINISH_2, null);
                        default:
                            return Pair.of(CLOSE_2, null);
                    }
                },
                () -> {
                    Output o = new Output();
                    o.FLOOR_2 = true;
                    o.LIGHT = true;
                    o.DOOR_2_CLOSE = true;
                    return o;
                }
        );
        CLOSE_3 = State.build("CLOSE_3", alphabet -> {
                    switch (alphabet) {
                        case DC_3:
                            return Pair.of(READY_3, 15);
                        case DI_3:
                        case OPEN:
                        case C_3:
                            return Pair.of(FINISH_3, null);
                        default:
                            return Pair.of(CLOSE_3, null);
                    }
                },
                () -> {
                    Output o = new Output();
                    o.FLOOR_3 = true;
                    o.LIGHT = true;
                    o.DOOR_3_CLOSE = true;
                    return o;
                }
        );
        CLOSE_4 = State.build("CLOSE_4", alphabet -> {
                    switch (alphabet) {
                        case DC_4:
                            return Pair.of(READY_4, 15);
                        case DI_4:
                        case OPEN:
                        case C_4:
                            return Pair.of(FINISH_4, null);
                        default:
                            return Pair.of(CLOSE_4, null);
                    }
                },
                () -> {
                    Output o = new Output();
                    o.FLOOR_4 = true;
                    o.LIGHT = true;
                    o.DOOR_4_CLOSE = true;
                    return o;
                }
        );
    }
}
