package cz.ti.elevator.automaton;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Represents CLK pulse generator,
 * run {@link #runnable} every {@link #PERIOD} ms
 * <p>
 * Created by Martin Forejt on 18.12.2018.
 * me@martinforejt.cz
 *
 * @author Martin Forejt
 */
public class CLK extends TimerTask {
    /**
     * Period in ms
     */
    private static final int PERIOD = 1000;
    /**
     * Runnable which may be executed every {@link #PERIOD} ms
     */
    private final Runnable runnable;
    /**
     * Timer
     */
    private Timer timer;

    /**
     * Constructs new {@link CLK}
     *
     * @param runnable runnable to be executed each {@link #PERIOD} ms
     */
    CLK(Runnable runnable) {
        this.runnable = runnable;
        timer = new Timer();
    }

    /**
     * Starts task
     */
    void start() {
        if (timer == null) {
            timer = new Timer();
        }
        timer.schedule(new CLK(runnable), 0, PERIOD);
    }

    /**
     * Stops task
     */
    void stop() {
        timer.cancel();
        timer = null;
    }

    /**
     * Runs {@link #runnable}
     */
    @Override
    public void run() {
        runnable.run();
    }
}
