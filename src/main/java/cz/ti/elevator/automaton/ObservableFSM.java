package cz.ti.elevator.automaton;

/**
 * {@link FSM} with callback
 * <p>
 * Created by Martin Forejt on 18.12.2018.
 * me@martinforejt.cz
 *
 * @author Martin Forejt
 */
public interface ObservableFSM extends FSM {

    /**
     * Sets on state change listener
     *
     * @param onStateChangedListener listener
     */
    void setOnStateChangedListener(OnStateChangedListener onStateChangedListener);

}
