package cz.ti.elevator.automaton;

/**
 * On state change listener
 * <p>
 * Created by Martin Forejt on 18.12.2018.
 * me@martinforejt.cz
 *
 * @author Martin Forejt
 */
public interface OnStateChangedListener {

    /**
     * Called when state changed
     *
     * @param output current state Moore output
     */
    void onStateChanged(Object output);

}
