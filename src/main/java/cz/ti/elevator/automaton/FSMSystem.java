package cz.ti.elevator.automaton;

/**
 * Main system including
 * {@link ElevatorFSM}, {@link TimerFSM} and {@link CLK}
 * <p>
 * Created by Martin Forejt on 18.12.2018.
 * me@martinforejt.cz
 *
 * @author Martin Forejt
 */
public class FSMSystem implements ObservableFSM {

    /**
     * Elevator FSM
     */
    private ElevatorFSM elevator;
    /**
     * Timer FSM
     */
    private TimerFSM timer;
    /**
     * CLK generates pulse each {@link CLK#PERIOD} ms
     */
    private CLK clk;
    /**
     * On state change listener
     */
    private OnStateChangedListener onStateChangedListener;

    /**
     * Creates all parts of system and start {@link #clk}
     */
    public FSMSystem() {
        elevator = new ElevatorFSM();
        timer = new TimerFSM();

        clk = new CLK(() -> {
            int out = timer.push(TimerInputAlphabet.CLK.name());
            if (out == 1) {
                push(ElevatorInputAlphabet.T.name());
            }
        });
        clk.start();
    }

    /**
     * Push input to {@link #elevator}
     * and the Mealy output from {@link #elevator} (converts to its alphabet and) push to {@link #timer}
     *
     * @param input input
     * @return Mealy output = 0
     */
    @Override
    public int push(String input) {
        int out = elevator.push(input);

        if (out == BAD_INPUT) {
            return BAD_INPUT;
        }

        timer.push(TimerInputAlphabet.byTime(out).name());
        if (onStateChangedListener != null) {
            onStateChangedListener.onStateChanged(getStateOutput());
        }
        return 0;
    }

    /**
     * Current Moore state output
     *
     * @return output
     */
    @Override
    public Object getStateOutput() {
        return elevator.getStateOutput();
    }

    /**
     * Reset all system parts
     */
    @Override
    public void reset() {
        elevator.reset();
        timer.reset();
        clk.stop();
        clk.start();
    }

    /**
     * Sets on state change listener
     *
     * @param onStateChangedListener listener
     */
    @Override
    public void setOnStateChangedListener(OnStateChangedListener onStateChangedListener) {
        this.onStateChangedListener = onStateChangedListener;
    }
}
