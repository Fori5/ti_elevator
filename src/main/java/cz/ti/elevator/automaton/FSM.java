package cz.ti.elevator.automaton;

/**
 * Finite state machine interface
 * <p>
 * Created by Martin Forejt on 17.12.2018.
 * me@martinforejt.cz
 *
 * @author Martin Forejt
 */
public interface FSM {

    int BAD_INPUT = -1;

    /**
     * Push input to FSM
     *
     * @param input input
     * @return current Mealy output
     */
    int push(String input);

    /**
     * Returns current Moore state output
     *
     * @return Moore output
     */
    Object getStateOutput();

    /**
     * Resets FSM and all its internal machines
     */
    void reset();

}
