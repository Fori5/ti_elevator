package cz.ti.elevator;

import cz.ti.elevator.automaton.FSMSystem;
import cz.ti.elevator.automaton.ObservableFSM;
import cz.ti.elevator.automaton.OnStateChangedListener;
import cz.ti.elevator.gui.GUI;
import cz.ti.elevator.simulation.Simulation;

import java.util.Arrays;
import java.util.Scanner;

import static cz.ti.elevator.automaton.FSM.BAD_INPUT;

/**
 * Main point of application
 * <p>
 * Can be run in "simulation" mode - default
 * Or "manual" mode - with "-man" argument
 * <p>
 * Created by Martin Forejt on 16.12.2018.
 * me@martinforejt.cz
 *
 * @author Martin Forejt
 */
public class Application {

    public static void main(String[] args) {
        ObservableFSM fsm;
        boolean manual = Arrays.asList(args).contains("-man");
        boolean cmd = Arrays.asList(args).contains("-cmd");

        if (manual) {
            fsm = new FSMSystem();
            System.out.println("Start of manual mode...");
        } else {
            System.out.println("Start of simulation mode...");
            fsm = new Simulation(new FSMSystem());
        }

        if (cmd) {
            fsm.setOnStateChangedListener(System.out::println);
        } else {
            GUI.getInstance(fsm);
        }

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter input: ");

        while (true) {

            String input = scanner.nextLine().trim();

            switch (input) {
                case "exit":
                    System.out.println("Application Closed");
                    scanner.close();
                    System.exit(0);
                    break;
                case "reset":
                    System.out.println("FSM reset done");
                    fsm.reset();
                    continue;
            }

            int out = fsm.push(input);
            if (out == BAD_INPUT) {
                System.out.println("Bad input");
            }
        }

    }

}
